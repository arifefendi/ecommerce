 <?php $this->load->view('admin/template/head');?>
                    <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Post member</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php 
                                   echo form_open('admin/member/post');
                                ?>
                                <form role="form">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Nama member</label>
                                            <input type="text" class="form-control" placeholder="Nama member" name="nama">
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Link</label>
                                            <input type="text" class="form-control" placeholder="Link" name="nama">
                                        </div>
                                        <div class="form-group">
                                            <label>Parent</label>
                                            <select name="parent" class="form-control">
                                                <option value="0">Parent Menu</option>
                                                <?php
                                                 foreach ($parent as $p) {
                                                    echo "<option value='$p->member_id'>$p->nama_member</option>";
                                                 }
                                                ?>
                                            </select>
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                        <?php 
                                        echo anchor('admin/member','Kembali',array('class'=>'btn btn-primary'));
                                        ?>
                                    </div>
                                </form>
                            </div><!-- /.box -->