 <?php $this->load->view('admin/template/head');?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data member</h3>
                                </div><!-- /.box-header -->
                                 <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Lengkap</th>
                                                <th>Email</th>
                                                <th>Alamat</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no=1;
                                            foreach ($record as $r) {
                                                echo "<tr>
                                                    <td width='26' align='center'>$no</td>
                                                    <td>$r->nama_lengkap</td>
                                                    <td>$r->email</td>
                                                    <td>$r->alamat</td>
                                                    <td width='10' height='20px'>".anchor("admin/member/detail/".$r->member_id,"<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>",array('title'=>'Edit data'))."</td>
                                                    <td width='10' height='20px'>".anchor("admin/member/delete/".$r->member_id,"<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>",array('title'=>'Hapus data'))."</td>
                                                </tr>";
                                                $no++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->