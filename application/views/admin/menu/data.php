 <?php $this->load->view('admin/template/head');?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Menu</h3>
                                </div><!-- /.box-header -->
                                <?php
                                    echo anchor('admin/menu/post','Input Menu Website',array('class'=>'btn btn-primary'));
                                ?>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Menu</th>
                                                <th>Jenis Menu</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no=1;
                                            foreach ($record as $r) {
                                                echo "<tr>
                                                    <td width='26' align='center'>$no</td>
                                                    <td>$r->menu_title</td>
                                                    <td>";
                                                    if($r->parent==0){
                                                        echo "Menu Utama";
                                                    }else{
                                                        $parent=$this->db->get_where('tabel_menu',array('menu_id'=>$r->parent))->row_array();
                                                        echo $parent['menu_title'];
                                                    }
                                                    echo"</td>
                                                    <td width='10' height='20px'>".anchor("admin/menu/edit/".$r->menu_id,"<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>",array('title'=>'Edit data'))."</td>
                                                    <td width='10' height='20px'>".anchor("admin/menu/delete/".$r->menu_id,"<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>",array('title'=>'Hapus data'))."</td>
                                                </tr>";
                                                $no++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->