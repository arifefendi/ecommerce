 <?php $this->load->view('admin/template/head');?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data transaksi</h3>
                                </div><!-- /.box-header -->
                                 <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pemesan</th>
                                                <th>Tanggal</th>
                                                <th>Status</th>
                                                <th>No. Resi</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no=1;
                                            foreach ($record as $r) {
                                                $status=$r->status==1?'PROSES':'SELESAI';
                                                echo "<tr>
                                                    <td width='26' align='center'>$no</td>
                                                    <td>$r->nama_lengkap</td>
                                                    <td>$r->tanggal</td>
                                                    <td>$status</td>
                                                    <td>$r->no_resi</td>
                                                    <td width='10' height='20px'>".anchor("admin/transaksi/detail/".$r->transaksi_id,"<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>",array('title'=>'Edit data'))."</td>
                                                    <td width='10' height='20px'>".anchor("admin/transaksi/delete/".$r->transaksi_id,"<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>",array('title'=>'Hapus data'))."</td>
                                                </tr>";
                                                $no++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                            <!-- /.box -->