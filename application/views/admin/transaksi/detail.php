 <?php $this->load->view('admin/template/head');?>
                            <!-- general form elements -->
                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <h3 class="box-title">Detail transaksi</h3>
                                        </div><!-- /.box-header -->
                                        <table class="table table-bordered">
                                            <tr><td width="200">nama Lengkap</td><td><?php echo $row['nama_lengkap'] ;?></td></tr>
                                            <tr><td>no Hp / Telp</td><td><?php echo $row['no_hp'] ;?> / <?php echo $row['no_telpon'] ;?></td></tr>
                                            <tr><td>email</td><td><?php echo $row['email'] ;?></td></tr>
                                            <tr><td>alamat</td><td><?php echo $row['alamat'] ;?></td></tr>
                                        </table>
                    
                                       <div class="box box-primary">
                                        <div class="box-header">
                                            <h3 class="box-title">Informasi transaksi</h3>
                                        </div><!-- /.box-header -->
                                        <?php
                                        echo form_open('admin/transaksi/detail');
                                        ?>
                                        <input type="hidden" name="id" value="<?php echo $info['transaksi_id'];?>">
                                        <table class="table table-bordered">
                                            <tr><td width="200">Tanggal Order</td><td><?php echo $info['tanggal'] ;?></td></tr>
                                            <tr><td>Status Order</td><td>
                                                <?php 
                                                $status=array(1=>'PROSES',2=>'SUDAH DIKIRIM');
                                                echo form_dropdown('status',$status,$info['status'],"class='form-control'")
                                                ;?>
                                            </td></tr>
                                            <tr><td>No. Resi</td><td><input type="text" name="resi" value="<?php echo $info['no_resi'] ;?>" class='form-control'</td></tr>
                                            <tr><td colspan="2"><button type="submit" name="submit" class="btn btn-danger btn-sm">Simpan Perubahan</button></td></tr>
                                        </table>
                                    </form>
                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <h3 class="box-title">Riwayat Transaksi</h3>
                                        </div>

                                        <table class="table table-bordered">
                                            <tr><th width="30">No.</th>
                                                <th>Nama Product</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                                <th>Total</th>
                                            </tr>
                                        <?php
                                        $no = 1;
                                        $total = 0;
                                        foreach ($order as $o) { 
                                            echo "<tr>
                                                    <td>$no</td> 
                                                    <td>$o->nama_product</td>
                                                    <td>$o->qty</td>
                                                    <td>$o->harga</td>
                                                    <td>".$o->harga*$o->qty."</td>
                                                 </tr>";
                                            $total=$total+($o->harga*$o->qty);
                                            $no++;

                                                }
                                            ?>
                                            <tr><td colspan="4"><center><b> T O T A L</b></th></center>

                                                    <td>
                                                        <?php echo $total; ?>
                                                    </td>
                                            </td></tr>
                            
                                        </table>
                                    </div>

                                    </div><!-- /.box -->