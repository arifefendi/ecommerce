 <?php $this->load->view('admin/template/head');?>
                    <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Post Product</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php 
                                   echo form_open_multipart('admin/product/post');
                                ?>
                                <form role="form">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Nama Product</label>
                                            <input type="text" class="form-control" placeholder="Nama product" name="nama_product">
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Harga</label>
                                            <input type="text" class="form-control" placeholder="Harga" name="harga">
                                        </div>
                                        <div class="form-group">
                                            <label>Gambar </label>
                                            <input type="file" name="userfile">
                                        </div>
                                        <div class="form-group">
                                            <label>Kategori </label>
                                            <select name="kategori" class="form-control">
                                                <?php
                                                 foreach ($kategori as $k) {
                                                    echo "<option value='$k->kategori_id'>$k->nama_kategori</option>";
                                                 }
                                                ?>
                                            </select>
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                        <?php 
                                        echo anchor('admin/kategori','Kembali',array('class'=>'btn btn-primary'));
                                        ?>
                                    </div>
                                </form>
                            </div><!-- /.box -->
