 <?php $this->load->view('admin/template/head');?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Product</h3>
                                </div><!-- /.box-header -->
                                <?php
                                    echo anchor('admin/product/post','Input Product',array('class'=>'btn btn-primary'));
                                ?>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Product</th>
                                                <th>Harga</th>
                                                <th>Kategori</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no=1;
                                            foreach ($record as $r) {
                                                echo "<tr>
                                                    <td width='26' align='center'>$no</td>
                                                    <td>". strtoupper($r->nama_product)."</td>
                                                    <td width='100'>$r->harga</td>
                                                    <td width='200'>$r->nama_kategori</td>
                                                    <td width='10' height='20px'>".anchor("admin/product/edit/".$r->product_id,"<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>",array('title'=>'Edit data'))."</td>
                                                    <td width='10' height='20px'>".anchor("admin/product/delete/".$r->product_id,"<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>",array('title'=>'Hapus data'))."</td>
                                                </tr>";
                                                $no++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->