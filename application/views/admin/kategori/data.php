 <?php $this->load->view('admin/template/head');?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Kategori</h3>
                                </div><!-- /.box-header -->
                                <?php
                                    echo anchor('admin/kategori/post','Input Kategori',array('class'=>'btn btn-primary'));
                                ?>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Kategori</th>
                                                <th>Jenis</th>
                                                <th>Jumlah Barang</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no=1;
                                            foreach ($record as $r) {
                                                echo "<tr>
                                                    <td width='26' align='center'>$no</td>
                                                    <td>$r->nama_kategori</td>
                                                    <td>";
                                                    if($r->parent==0){
                                                        echo "Kategori Utama";
                                                    }else{
                                                        $parent=$this->db->get_where('tabel_kategori',array('kategori_id'=>$r->parent))->row_array();
                                                        echo strtoupper($parent['nama_kategori']);
                                                    }
                                                    echo"</td>
                                                    <td></td>
                                                    <td width='10' height='20px'>".anchor("admin/kategori/edit/".$r->kategori_id,"<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>",array('title'=>'Edit data'))."</td>
                                                    <td width='10' height='20px'>".anchor("admin/kategori/delete/".$r->kategori_id,"<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>",array('title'=>'Hapus data'))."</td>
                                                </tr>";
                                                $no++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->