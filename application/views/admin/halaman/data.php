 <?php $this->load->view('admin/template/head');?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Halaman Statis</h3>
                                </div><!-- /.box-header -->
                                <?php
                                    echo anchor('admin/halaman/post','Input Halaman Statis',array('class'=>'btn btn-primary'));
                                ?>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Judul</th>
                                                <th>Link</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no=1;
                                            foreach ($record as $r) {
                                                echo "<tr>
                                                        <td width='26' align='center'>$no</td>
                                                        <td>$r->judul</td>
                                                        <td>".base_url()."p/$r->judul_seo</td>
                                                        </td>
                                                        <td width='10' height='20px'>".anchor("admin/halaman/edit/".$r->pages_id,"<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>",array('title'=>'Edit data'))."</td>
                                                        <td width='10' height='20px'>".anchor("admin/halaman/delete/".$r->pages_id,"<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>",array('title'=>'Hapus data'))."</td>
                                                     </tr>";
                                                $no++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
