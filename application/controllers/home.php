<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	function __construct() {
		parent::__construct();
		$this->load->model('mod_front');
	}

	public function index()	{
		$data['product']= $this->mod_front->getProduct(); 
		$this->template->load('template','home',$data);
	}
}